# ChatGPT for Siri

 _更新时间：2023/8/6_
 
 _最新版本：v4.1_ 


#### 介绍
这是一个Siri快捷指令（Shortcut），基于OpenAI GPT 3.5，可实现与Apple设备进行中英文连续对话。支持文字、语音交流和共享表单调用。

重点：完全免费，无需账号，国内可用！

#### 最新下载地址
[点击此处下载最新“智能助手”快捷指令](https://www.icloud.com/shortcuts/fe19c498c8c141f9b06bbb73f05b5dd3)

#### 支持设备
只支持苹果设备（iPhone，Apple Watch，iPad，CarPlay，HomePod等）

#### 使用教程

1.  点击上述链接下载最新快捷指令
2.  安装指令时，在配置页面，您可以自定义API站点（支持官方站点），如果不填写API站点，则默认使用指令自带的免费站点
3.  在配置页面，如果`GPT_App`设为`False`（默认），则自动调用第三方免费API或自定义的API与ChatGPT进行通信，无需账号，国内可用
4.  如果`GPT_App`设为`True`，代表使用您自己的ChatGPT账号，则请先下载ChatGPT官方App（需外区AppStore账号），并在App中登录ChatGPT账号，且需下载小火箭并配置好IP（HK不行，美区最佳）
5.  手动运行一次本指令，并允许所有权限
6.  对Apple设备说“Hey Siri，智能助手”
7.  对话过程中，如果想提前结束或终止，对Siri说“结束”、“再见”、“Bye”都可以

本指令支持手写、语音、共享表单调用

#### 打赏作者
[点我，请作者喝咖啡，感谢鼓励~](https://docs.qq.com/doc/DRHlETnpWdk1lWkV1)